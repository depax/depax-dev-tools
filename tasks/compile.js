/**
 * Provides the TypeScript compile tasks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

const gulp = require("gulp");
const clean = require("gulp-clean");
const ts = require("gulp-typescript");

/** Perform unit testing and code coverage. */
module.exports = (pkg, gulp) => {
    // Clean the distribution folder.
    gulp.task("compile:clean", () => gulp.src([ "dist" ], {
        allowEmpty: true,
        read: false,
    }).pipe(clean()));

    /** Compile the TypeScript files, and output to 'dist' folder. */
    gulp.task("compile:build", () => {
        const tsp = ts.createProject("tsconfig.json");
        const result = tsp.src().pipe(tsp());

        result.dts.pipe(gulp.dest("dist"));
        return result.js.pipe(require("gulp-babel-minify")({
            mangle: { keepClassNames: true },
        })).pipe(gulp.dest("dist"));
    });

    /** Perform the build operations. */
    return gulp.series("compile:clean", "compile:build");
}
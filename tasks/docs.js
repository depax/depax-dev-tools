/**
 * Provides the documentation gulp tasks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

const clean = require("gulp-clean");
const typedoc = require("gulp-typedoc");
const os = require("os");
const path = require("path");
const url = require("url");

/** Generate documentation. */
module.exports = (pkg, gulp) => {
    const pkgName = pkg.name.replace("@depax/", "");

    let repoUrl = pkg.repository.url;
    if (repoUrl.indexOf("https://") === 0) {
        const parsedUrl = new url.URL(repoUrl);
        repoUrl = `git@${parsedUrl.host.replace("www.", "")}:${parsedUrl.pathname.substr(1)}.git`;
    }

    const srcDir = path.join(".", "src", "**", "*.ts");
    const docsDir = path.join(".", ".build", "docs");
    const reportsDir = path.join(".", ".build", "reports");
    const tmpDir = path.join(os.tmpdir(), pkgName);
    const docsMarkdownDir = `${tmpDir}-markdown-doc`;

    /** Clean out the docs coverage and test reports. */
    gulp.task("docs:clean", () => gulp.src([ docsDir, tmpDir, docsMarkdownDir ], {
        allowEmpty: true,
        read: false,
    }).pipe(clean({ force: true })));

    /** The task to build the documentation. */
    gulp.task("docs:build", () => gulp
        .src([srcDir])
        .pipe(typedoc({
            module: "commonjs",
            target: "es2016",
            includeDeclarations: true,
            out: docsDir,
            json: path.join(reportsDir, "documentation.json"),
            name: pkg.title || pkgName,
            theme: "default",
            ignoreCompilerErrors: false,
            version: true,
            plugin: [ "none" ],
            excludeExternals: true,
        })));

    return gulp.series("docs:clean", "docs:build");
}

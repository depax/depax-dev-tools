/**
 * @todo
 */

"use strict";

const cp = require("child_process");
const fs = require("fs");
const ncp = require("ncp");
const rimraf = require("rimraf");

/**
 * peforms a child process exec and returns a promise.
 */
module.exports.exec = function(cmd, cwd) {
    return new Promise((resolve, reject) => {
        cp.exec(cmd, { cwd: cwd || process.cwd() }, (err, stdout, stderr) => err ? reject(err) : resolve());
    });
}

/**
 * Deletes a given directory.
 */
module.exports.deldir = function(dir) {
    return new Promise((resolve, reject) => rimraf(dir, (err) => err ? reject(err) : resolve()));
}

/**
 * Deletes a given file.
 */
module.exports.delFile = function(filename) {
    return new Promise((resolve, reject) => fs.unlink(filename, (err) => err ? reject(err) : resolve()));
}

/**
 * Copies the contents of one directory to another.
 */
module.exports.copydir = function(source, target) {
    return new Promise((resolve, reject) => ncp.ncp(source, `${target}/`, (err) => err ? reject(err) : resolve()));
}

/**
 * Renames a given file.
 */
module.exports.rename = function(source, target) {
    return new Promise((resolve, reject) => fs.rename(source, target, (err) => err ? reject(err) : resolve()));
}
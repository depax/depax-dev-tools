/**
 * Provides the generic gulp tasks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

module.exports = (pkg, gulp) => {
    gulp.task("test", require("./tasks/test")(pkg, gulp));
    gulp.task("compile", require("./tasks/compile")(pkg, gulp));
    gulp.task("shields", require("./tasks/shields")(pkg, gulp));
    gulp.task("docs", require("./tasks/docs")(pkg, gulp));

    gulp.task("default", gulp.series("compile", "test", "docs", "shields"));
}

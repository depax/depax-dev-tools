/**
 * Provides the shields gulp tasks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

const Generate = require("@depax/shield").Generate;
const fs = require("fs");
const path = require("path");
const clean = require("gulp-clean");

/**
 * A util function to aid in generating the shields.
 *
 * @param {string} label The label of the shield.
 * @param {string} value The value of the shield.
 * @param {number} pct A number between 0->100, 0-79 = red, 80-99 = orange, and 100 = green.
 * @param {string} target The target filename.
 */
async function generateShield(dir, label, value, pct, target) {
    const Generate = require("@depax/shield").default;
    let style;

    if (pct === 100) {
        style = require("@depax/shield").GreenShieldStyle;
    } else if (pct >= 80) {
        style = require("@depax/shield").YellowShieldStyle;
    } else {
        style = require("@depax/shield").RedShieldStyle;
    }

    await Generate(label, value, path.join(`${dir}/${target}.svg`), style);
}

/** Perform unit testing and code coverage. */
module.exports = (pkg, gulp) => {
    const reportsDir = path.join(".", ".build", "reports");
    const shieldsDir = path.join(".", ".build", "shields");

    const todosReport = path.join(reportsDir, "todos.json");
    const featuresReport = path.join(reportsDir, "features.json");
    const featuresCoverageReport = path.join(reportsDir, "coverage-summary.json");

    /** Clean out the shields directory. */
    gulp.task("shields:clean", () => gulp.src([ shieldsDir ], {
        allowEmpty: true,
        read: false,
    }).pipe(clean({ force: true })));

    /** Create the shields directory. */
    gulp.task("shields:dir", (done) => fs.mkdir(shieldsDir, done));

    /** Create the TODOs shield. */
    gulp.task("shields:todo", async () => {
        if (fs.existsSync(todosReport) === false) {
            return;
        }

        const report = JSON.parse(fs.readFileSync(todosReport).toString());
        let total = report.length;
        const counts = {};

        report.forEach((item) => {
            const kind = (item.kind || item.tag).toLowerCase();
            if (!counts[kind]) {
                counts[kind] = 0;
            }

            counts[kind]++;
        });

        return await generateShield(
            shieldsDir,
            "Todos",
            `${counts.todo || 0} todos, ${counts.fixme || 0} fixmes`,
            total === 0 ? 100 : 80,
            "todos",
        );
    });

    /** Create the report for features. */
    gulp.task("shields:features", async () => {
        if (fs.existsSync(featuresReport) === false) {
            return;
        }

        let report = JSON.parse(fs.readFileSync(featuresReport).toString());
        let passed = 0;
        let total = 0;

        report.forEach((feature) => {
            feature.elements.forEach((scenario) => {
                scenario.steps.forEach((step) => {
                    if (step.hidden) {
                        return;
                    }

                    total++;
                    if (step.result.status === "passed") {
                        passed++;
                    }
                });
            });
        });

        await generateShield(
            shieldsDir,
            "Features",
            `${passed} passed out of ${total}`,
            passed === total ? 100 : ((100 * passed) / total).toFixed(2),
            "features",
        );

        report = JSON.parse(fs.readFileSync(featuresCoverageReport).toString());
        total = report.total.lines.total + report.total.statements.total + report.total.functions.total;
        const covered = report.total.lines.covered + report.total.statements.covered + report.total.functions.covered;
        const skipped = report.total.lines.skipped + report.total.statements.skipped + report.total.functions.skipped;

        return await generateShield(
            shieldsDir,
            "Coverage",
            `${total} total, ${covered} covered, ${skipped} skipped`,
            total / covered * 100,
            "features-coverage",
        );
    });

    return gulp.series([ "shields:clean", "shields:dir", "shields:todo", "shields:features" ]);
}

/**
 * Provides the test gulp tasks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

const fs = require("fs");
const mkdirp = require("mkdirp");
const path = require("path");
const run = require("gulp-run");
const tslint = require("gulp-tslint");

/** Perform unit testing and code coverage. */
module.exports = (pkg, gulp) => {
    const pkgName = pkg.name.replace("@depax/", "");

    const reportsDir = path.join(".", ".build", "reports");
    const binDir = path.join(".", "node_modules", ".bin");
    const srcDir = path.join(".", "src", "**", "*.ts");
    const testsDir = path.join(".", "tests", "**", "*.spec.ts");
    const featuresDir = path.join(".", "features");

    const featuresFilename = path.join(reportsDir, "features.json");
    const todosFilename = path.join(reportsDir, "todos.json");
    const tslintFilename = path.join(reportsDir, "tslint.json");

    /** Clean out the reports and output directories. */
    gulp.task("test:clean", () => gulp.src([ reportsDir ], {
        allowEmpty: true,
        read: false,
    }).pipe(require("gulp-clean")({ force: true })));

    /** Create the reports directory. */
    gulp.task("test:dir", (done) => mkdirp(reportsDir, () => done()));

    /** Perform the code linting. */
    gulp.task("test:tslint", () => {
        const tsp = require("gulp-typescript").createProject("tsconfig.json");
        const tslp = require("tslint").Linter.createProgram("tsconfig.json", ".");

        return tsp.src().pipe(tslint({
            formatter: "stylish",
            configuration: path.join(".", "tslint.json"),
            program: tslp
        })).pipe(tslint.report({
            emitError: false,
            summarizeFailureOutput: true
        }));
    });

    /** Perform the code linting. */
    gulp.task("test:tslint:report", (done) => {
        const tsp = require("gulp-typescript").createProject("tsconfig.json");
        const tslp = require("tslint").Linter.createProgram("tsconfig.json", ".");

        const report = {
            totals: {
                errors: 0,
                warnings: 0
            },
            items: []
        };

        tsp.src().pipe(tslint({
                // formatter: "json",
                configuration: path.join(".", "tslint.json"),
                program: tslp
            }))
            .pipe(require("map-stream")((file, done) => {
                file.tslint.failures.forEach((failure) => {
                    report.items.push({
                        filename: failure.fileName,
                        line: failure.startPosition.lineAndCharacter,
                        ruleName: failure.ruleName,
                        message: failure.failure,
                        severity: failure.ruleSeverity
                    });
                });

                report.totals.errors += file.tslint.errorCount;
                report.totals.warnings += file.tslint.warningCount;

                done(null, file);
            })).on("end", () => {
                fs.writeFileSync(tslintFilename, JSON.stringify(report, null, 4));
                done();
            });
    });

    /** Test for any TODOs and FIXMEs. */
    gulp.task("test:todos", () => gulp.src([srcDir, testsDir]).pipe(require("gulp-todo")({
        fileName: "todos.json",
        reporter: "json"
    })).pipe(gulp.dest(reportsDir)));

    /** Perform the cucumber tests. */
    gulp.task("test:cucumber", (done) => {
        const nycArgs = [];
        nycArgs.push(`--cwd=./src`);
        nycArgs.push("--include=**/*.ts");
        nycArgs.push("--extension=.ts");
        nycArgs.push("--require=ts-node/register");
        nycArgs.push("--reporter=text-summary");
        nycArgs.push("--reporter=json");
        nycArgs.push("--reporter=json-summary");
        nycArgs.push(`--report-dir=../${reportsDir}`);
        nycArgs.push("--source-map=true");
        nycArgs.push("--instrument=true");
        nycArgs.push("--all=true");

        const cucumberArgs = [];
        cucumberArgs.push(`--require ./features/_support/**/*.ts`);
        cucumberArgs.push("--compiler ts:ts-node/register");
        cucumberArgs.push(`--format json:${featuresFilename}`);
        cucumberArgs.push(featuresDir);

        return run(`${path.join(binDir, "nyc")} ${nycArgs.join(" ")} ${path.join(binDir, "cucumber-js")} ${cucumberArgs.join(" ")}`, {
            verbosity: 3,
        })
        .exec((err) => done(err));
    });

    gulp.task("test:report", (done) => {
        console.info("Generate final report.");
        const readme = path.normalize(path.join(process.cwd(), "README.md"));
        const cfg = {
            output: path.join(reportsDir, "report.html"),
            brand: "DePax",
            project: pkg.title || pkgName,
            syntaxTheme: "okaidia",
            reports: [],
        };

        if (fs.existsSync(readme)) {
            cfg.readme = readme;
        }

        if (fs.existsSync(todosFilename)) {
            cfg.reports.push({
                renderer: "todos",
                config: {
                    filename: todosFilename,
                    title: "Todo",
                },
            });
        }

        if (fs.existsSync(tslintFilename)) {
            cfg.reports.push({
                renderer: "tslint",
                config: {
                    filename: tslintFilename,
                    title: "TSLint",
                },
            });
        }

        if (fs.existsSync(featuresFilename)) {
            cfg.reports.push({
                renderer: "cucumber",
                config: {
                    filename: featuresFilename,
                    title: "Features",
                },
            });
        }

        if (fs.existsSync(path.join(reportsDir, "coverage-final.json"))) {
            cfg.reports.push({
                renderer: "code-coverage",
                config: {
                    filename: path.join(reportsDir, "coverage-final.json"),
                    title: "Coverage",
                }
            });
        }

        const Reporter = require("depax-reporter").default;
        const reporter = new Reporter(cfg);
        reporter.render()
            .then(() => done())
            .catch((err) => done(err));
    });

    const jobs = [];
    jobs.push("test:clean", "test:dir", "test:tslint", "test:tslint:report", "test:todos");
    if (fs.existsSync(path.join(".", "features"))) {
        jobs.push("test:cucumber");
    }

    jobs.push("test:report");
    return gulp.series(jobs);
}
